import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View , TextInput, Button} from 'react-native';
import Home from './components/Home'
import {Image} from "react-native-web";

export default function App() {
  return (
    <View style={styles.container}>
      <View style={styles.statusbar}>

      </View>
      <View style={styles.header}>
        <Text style={styles.title}>
          Myapp
        </Text>
      </View>
      <View style={styles.body}>
        <Text style={styles.khadija}>
          Khadija
        </Text>
        <Home style={styles.home} issam='Votre Nom' titre='Enregister'/>
      </View>
      <View style={styles.footer}>

      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    flexDirection: 'column', //Main  axis [row or column]
    justifyContent:'center',
    alignItems:'stretch' //Element distribution on the cross axis


  },
  statusbar:{
    height:32,
    backgroundColor: 'gray'
  },
  header: {
    height: 50,
    backgroundColor: 'blue',
    justifyContent:'center',
    alignItems:'center',


  },
  body: {
    flex:1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white'
  },
  footer: {
    height:50,
    backgroundColor:'blue'
  },
  home:{
    width:'100%'
  },
  khadija:{
    position:'absolute',
    bottom:'20%',
    right:30,
  },
  title : {
    color:'white',
    fontSize:24,
    fontWeight:'600'
  }
});
