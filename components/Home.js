import React from 'react';
import {Button, StyleSheet, Text, TextInput, View, Alert} from 'react-native';


export default class Home extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            name :"",
            counter:0
        }
    }

    buttonPressed = () => {
        const previous = this.state.counter
        Alert.alert(this.state.name)
        this.setState({

            counter : previous+1
        })
    }
    textChanged = (text) => {

        this.setState({
            name:text,

        })
    }
    render() {
        return(
            <View style={styles.main}>
                <Text>
                    {this.state.counter}
                </Text>
                <TextInput style={styles.nom}
                           onChangeText={this.textChanged}
                           placeholder={this.props.issam}/>
                <Button title={this.props.titre}
                        onPress={this.buttonPressed}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    main:{
        justifyContent:'center',
        alignItems:'center',
        width: '100%'
    },
    nom: {
        borderWidth:1,
        width:'70%',
        height:50,
        borderRadius:10

    }
})


// Props contain immutable properties of a given component.
// State contain properties that can change at any given time in the lifecycle of the component
